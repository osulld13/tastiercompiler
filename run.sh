#!/bin/bash

cd TastierCompiler
make

cd ../TastierMachine
cabal update
cabal configure
cabal build

cd ../TastierCompiler
mono bin/tcc.exe test/Programs/Test.TAS test.asm
~/TastierCompiler/TastierMachine/dist/build/tasm/tasm test.asm test.bc
~/TastierCompiler/TastierMachine/dist/build/tvm/tvm test.bc test/Inputs/test.IN
